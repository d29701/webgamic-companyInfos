package webmagic.commons.dao;

import org.apache.ibatis.annotations.Insert;
import webmagic.commons.model.WebPage;

/**
 * Created by dengsg on 2016/8/16 0016.
 */
public interface WebPageDao{
    @Insert("insert into company_info_web_page " +
        "(name,source,created_at,status,source_url,headers,html) " +
        "values " +
        "(#{name},#{source},#{date},#{status},#{sourceUrl},#{headers},#{html}   )")
    int insertWebPage(WebPage webPage);
}
