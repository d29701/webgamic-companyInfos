package webmagic.commons.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import webmagic.commons.model.CompanyUrl;

import java.util.List;

/**
 * Created by dengshougang on 16/8/4.
 */

public interface CompanyUrlDao {
   
    @Insert("insert into company_url_dsg (name,url,date,keyword,page_number,source,info_url) values (#{name},#{url},#{date},#{keyword},#{pageNumber},#{source},#{infoUrl}   ) ")
    int putCompanyUrl(CompanyUrl companyUrl);

    
    @Select("select company_name as name from company_profile where id >= 890403  and (location like '%深圳%' or company_name like '%深圳%')")
    List<CompanyUrl> selectActiveCalculateCompanyName();
    
    @Select("select company_name as name from company_profile where source not like #{0} and `1688`!=0 limit #{1},#{2}")
    List<CompanyUrl> selectAllUncrawledCompanyName(String key,int index,int range);
    
    @Select("select company_name as name from company_profile limit #{0},#{1}")
    List<CompanyUrl> selectAllCompanyName(int index,int range);
    
    @Select("select `name`,`url` from company_url_dsg where keyword like \"1688active%\" order by `date` limit #{0}," +
        "#{1}")
    List<CompanyUrl> select1688ActiveCompanyUrls(int begin,int range);


    @Select("select `company_name` as name from company_profile where is_sample is null and predicted is null and come_from_crm != 1 and (location like '%深圳%' or company_name like '%深圳%') and id <= 890403")
    List<CompanyUrl> select3rdCalculateCompanyNames();
    
}


