package webmagic.commons.dao;


import org.apache.ibatis.annotations.Update;
import webmagic.commons.model.CompanyInfo;

/**
 * Created by dengsg on 2016/8/9 0009.
 */
public interface CompanyInfoDao{
//    @Insert("insert into company_info (name,source_url,reg_capital,description,address,product,industry,operation_model,main_customers,monthly_output,annual_exports,brand,plant_area,employee_num,created_at,annual_sales,market,representative,source)  " +
//        "values (#{companyName},#{infoUrl},#{registeredCapital},#{businessScope},#{address},#{mainProducts},#{industry},#{businessModel},#{mainCustomers},#{monthlyOutput},#{annualExports},#{brand},#{plantArea},#{employees},#{date},#{annualSales},#{market},#{legalPerson},#{source}   )")
//        +"on DUPLICATE KEY UPDATE "+
//        "source_url=#{infoUrl},source=\"huicong\",reg_capital=#{registeredCapital},description=#{businessScope},address=#{address},product=#{mainProducts},industry=#{industry},operation_model=#{businessModel},main_customers=#{mainCustomers},monthly_output=#{monthlyOutput},annual_exports=#{annualExports},brand=#{brand},plant_area=#{plantArea},employee_num=#{employees},created_at=#{date},annual_sales=#{annualSales},market=#{market};")
    int insertCompanyInfo(CompanyInfo companyInfo);
    
    @Update("update company_info set `1688` = 1 where source like \"1688%\"")
    int updateAll1688CompanyBit1();
    
    @Update("update company_info set `hc360` =1 where source like \"huicong\"")
    int updateAllHuicongCompanyBit();
}
