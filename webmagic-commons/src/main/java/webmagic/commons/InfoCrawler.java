package webmagic.commons;

/**
 * Created by dengshougang on 16/8/24.
 */
public interface InfoCrawler{
    void crawlRedisCompanyInfos(String redisDBName,int number, int range,boolean ifPop);
    
}
