package webmagic.commons.pipeline;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;
import webmagic.commons.dao.CompanyInfoDao;
import webmagic.commons.dao.WebPageDao;
import webmagic.commons.model.CompanyInfo;
import webmagic.commons.model.WebPage;

import javax.annotation.Resource;

/**
 * Created by dengsg on 2016/8/8 0008.
 */
@Slf4j
@Component
public class CompanyInfoPipeline implements Pipeline{
    @Resource
    CompanyInfoDao companyInfoDao;
    @Resource
    WebPageDao webPageDao;
    
    @Override
    public void process(ResultItems resultItems, Task task){
        CompanyInfo info=resultItems.get("companyInfo");
        WebPage webPage=resultItems.get("webPage");
        try{
            if (webPage!=null&&StringUtils.isNotBlank(webPage.getName())){
                webPageDao.insertWebPage(webPage);
                log.info("success insert web page");
            }
        } catch (Exception e){
            if (e.getMessage().contains("Duplicate entry")){
//                log.info("Duplicate entry:{}", info.toString());
            } else{
                e.printStackTrace();
            }
        }
        try{
            if (StringUtils.isNotBlank(info.getCompanyName())) {
                companyInfoDao.insertCompanyInfo(info);
                log.info("success insert company: {}", info.toString());
            }
        } catch (Exception e ){
            if(e.toString().contains("Duplicate entry")){
                log.info("Duplicate entry {}",info.getCompanyName());
            }
            else {
                log.warn(e.toString());
            }
        }
    }
}
