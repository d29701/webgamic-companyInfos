package webmagic.commons.pipeline;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;
import webmagic.commons.dao.CompanyUrlDao;
import webmagic.commons.model.CompanyUrl;

import javax.annotation.Resource;

/**
 * Created by dengshougang on 16/8/17.
 */
@Slf4j
@Component
public class BaiduUrlPipeline implements Pipeline{
    @Resource
    protected CompanyUrlDao companyUrlDao;
    
    @Override
    public void process(ResultItems resultItems, Task task){
        if (resultItems==null){
            return;
        }
        CompanyUrl companyUrl=resultItems.get("companyUrl");
        if (companyUrl==null||StringUtils.isBlank(companyUrl.getUrl())||org.apache.commons.lang3.StringUtils.isBlank(companyUrl.getName())){
            return;
        }
        companyUrlDao.putCompanyUrl(companyUrl);
        log.info("sucess insert {}", companyUrl.toString());
        
    }
}
