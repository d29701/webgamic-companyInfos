package webmagic.commons.pipeline;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;
import webmagic.commons.dao.CompanyUrlDao;
import webmagic.commons.model.CompanyUrl;

import javax.annotation.Resource;
import java.util.Set;

/**
 * Created by dengshougang on 16/8/4.
 */
@Slf4j
@Component("CompanyUrlPipeline")
public class CompanyUrlPipeline implements Pipeline{
    @Resource
    private CompanyUrlDao companyUrlDao;
    
    @Override
    public void process(ResultItems resultItems, Task task){
        Set set=resultItems.get("company");
        if (set!=null) {
            for (Object c : set){
                try{
                    companyUrlDao.putCompanyUrl((CompanyUrl) c);
                } catch (Exception e){
                    if (e.toString().contains("Duplicate entry")) {
                        log.info("Duplicate entry {}", ((CompanyUrl) c).getName());
                    } else if (e.toString().contains("Data too long")) {
                        log.warn("company name too long: {} {}", ((CompanyUrl) c).getName(), ((CompanyUrl) c).getUrl());
                    } else log.warn("SQL Exception:\n{}", e);
                }
            }
        }
    }
}
