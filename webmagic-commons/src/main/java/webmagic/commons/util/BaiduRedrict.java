package webmagic.commons.util;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;

/**
 * Created by dengshougang on 16/8/18.
 */
public class BaiduRedrict{
    
    public synchronized static String redrict(String url){
        // TODO Auto-generated method stub
        CloseableHttpClient client = HttpClients.createDefault();
    
        //使用Get方式请求
        HttpGet httpget = new HttpGet(url);
//        HttpParams params = client.getParams();
//        params.setParameter(AllClientPNames.HANDLE_REDIRECTS, false);
        HttpParams params = new BasicHttpParams();
        params.setParameter("http.protocol.handle-redirects", false);
        httpget.setParams(params);
        //执行请求
        String rs=null;
        try {
            HttpResponse response = client.execute(httpget);
            int statusCode=response.getStatusLine().getStatusCode();
            if(statusCode==301||statusCode==302)
            {
                Header[] hs = response.getHeaders("Location");
                for(Header h:hs)
                {
//                    System.out.println(h.getName()+"--"+h.getValue());
                    rs=h.getValue();
                }
            }
        } catch (ClientProtocolException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return rs;
    }
    
    public static void main(String[] args){
        System.out.println(redrict("http://www.baidu.com/link?url=ZGEmneqAL2wzPq2k2HKvEVxCspa-9K5nVw_lXiDUBDq6VWp_tgGpJFrY5k7UDBMS"));
    }
}
