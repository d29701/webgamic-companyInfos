package webmagic.commons.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by dengshougang on 16/8/17.
 */
public class JedisUtil {
    
    private static JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), "172.31.105.22", 6379,10000 );
    
    
    public static final String _1688CalculateCompanyName="set_1688_calculate_company_name";
    public static final String _1688AllCompanyName="set_1688_all_company_name";
    
    
    
    public static final String hc360CalculateCompanyName="set_hc360_calculate_company_name";
    
    public static final String _1688_3rdCalculateCompanyName="set_1688_3rd_calculate_company_name";
    
    public static final String hc360_3rdCalculateCompanyName="set_hc360_3rd_calculate_company_name";
    
    
    
    public static JedisPool getJedisPool() {
        return jedisPool;
    }
    
    public static Jedis getJedis() {
        return getJedisPool().getResource();
    }
    
    public synchronized static void returnJedis(Jedis jedis){
        jedisPool.returnResource(jedis);
    }
    
}

