package webmagic.commons.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dengshougang on 16/8/17.
 */
public class StringUtils {
    public synchronized static String replaceBlank(String str) {
        String dest = "";
        if (str!=null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }
}