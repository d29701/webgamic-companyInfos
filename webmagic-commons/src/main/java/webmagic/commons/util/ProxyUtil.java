package webmagic.commons.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import us.codecraft.webmagic.Site;

import java.io.IOException;

/**
 * Created by dengsg on 2016/8/8 0008.
 */
@Slf4j
public class ProxyUtil{
    private static CloseableHttpClient httpClient= HttpClients.createDefault();

    private static HttpGet httpGet=new HttpGet("http://dps.kuaidaili.com/api/getdps/?orderid=907021314156220&num=1&sep=1");

    public synchronized static void changeProxy(Site site){
        String page=null;
        try (CloseableHttpResponse response = httpClient.execute(httpGet) ){
            page = IOUtils.toString(response.getEntity().getContent());
            String[] host=page.split(":");
            site.setHttpProxy(new HttpHost(host[0],Integer.parseInt(host[1])));
            log.warn("change proxy to {}",page);
        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException|NumberFormatException e){
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

    }
}
