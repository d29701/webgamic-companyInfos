package webmagic.commons.downloader.htmlunit;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

;

/**
 * Created by fengpj on 2016/7/29.
 * 自定义下载连接池
 */
public class WebClientPool {
    private Logger logger;
    private static final int DEFAULT_CAPACITY = 5;
    private final int capacity;
    private static final int STAT_RUNNING = 1;
    private static final int STAT_CLODED = 2;
    private AtomicInteger stat;
    private final List<WebClient> webClientList;
    private BlockingDeque<WebClient> innerQueue;

    public WebClientPool(int capacity) {
        this.logger = Logger.getLogger(this.getClass());
        this.stat = new AtomicInteger(1);
        this.webClientList = Collections.synchronizedList(new ArrayList<>());
        this.innerQueue = new LinkedBlockingDeque<>();
        this.capacity = capacity;
    }

    public WebClientPool() {
        this(DEFAULT_CAPACITY);
    }

    public WebClient get() throws InterruptedException {
        this.checkRunning();
        WebClient poll = this.innerQueue.poll();
        if(poll != null) {
            return poll;
        } else {
            if(this.webClientList.size() < this.capacity) {
                synchronized(this.webClientList) {
                    if(this.webClientList.size() < this.capacity) {
                        WebClient webClient = new WebClient(BrowserVersion.CHROME);
                        webClient.getOptions().setCssEnabled(false);
                        webClient.getOptions().setJavaScriptEnabled(true);
                        webClient.getOptions().setThrowExceptionOnScriptError(false);
                        webClient.getOptions().setTimeout(20000);
                        webClient.setJavaScriptTimeout(20000);
                        this.innerQueue.add(webClient);
                        this.webClientList.add(webClient);
                    }
                }
            }
            return this.innerQueue.take();
        }
    }

    public void returnToPool(WebClient webClient) {
        this.checkRunning();
        this.innerQueue.add(webClient);
    }

    protected void checkRunning() {
        if(!this.stat.compareAndSet(1, STAT_RUNNING)) {
            throw new IllegalStateException("Already closed!");
        }
    }

    public void closeAll() {
        boolean b = this.stat.compareAndSet(1, STAT_CLODED);
        if(!b) {
            throw new IllegalStateException("Already closed!");
        } else {
            for (WebClient webClient : this.webClientList) {
                this.logger.info("Quit webClient" + webClient);
                webClient.close();
            }

        }
    }
}
