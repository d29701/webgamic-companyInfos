package webmagic.commons.downloader.htmlunit;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.downloader.Downloader;
import us.codecraft.webmagic.selector.PlainText;

import java.io.IOException;

/**
 * Created by fengpj on 2016/8/3.
 * 使用HtmlUnit抓取用js生成的页面
 */
public class HtmlUnitDownloader implements Downloader{

    private static final Logger LOGGER = LoggerFactory.getLogger(HtmlUnitDownloader.class);

    private WebClientPool webClientPool;

    public HtmlUnitDownloader() {
        this.webClientPool =new WebClientPool();
    }

    @Override
    public Page download(Request request, Task task) {
        String url = request.getUrl();

        WebClient webClient;
        try {
            webClient = webClientPool.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }

        HtmlPage htmlPage;
        webClient.waitForBackgroundJavaScript(20000);
    
        try {
            htmlPage = webClient.getPage(url);
            LOGGER.info("downloader page: {}", url);
        } catch (IOException e) {
            LOGGER.info("downloader page {}, error: {}", url, e);
            return null;
        }
        
        String asXml = htmlPage.asXml();
        Page page = new Page();
        page.setRawText(asXml);
        page.setUrl(new PlainText(request.getUrl()));
        page.setRequest(request);
        this.webClientPool.returnToPool(webClient);
        return page;
    }

    @Override
    public void setThread(int threadNum) {

    }
}
