package webmagic.commons;

import webmagic.commons.model.CompanyUrl;

import java.util.List;

/**
 * Created by dengshougang on 16/8/24.
 */
public interface UrlCrawler{
    void addCompanyNameToRedis(String redisDBName, List<CompanyUrl> ls);
    void crawlRedisCompanyUrl(String redisDBName, int number, int range,boolean ifPop);
}
