package webmagic.commons.model;

import lombok.Data;

import java.util.Date;

/**
 * Created by dengsg on 2016/8/16 0016.
 */
@Data
public final class WebPage{
    private String name=null;
    private Date date=new Date();
    private String source=null;
    private String sourceUrl;
    private int status=200;
    private String headers;
    private String html;
}
