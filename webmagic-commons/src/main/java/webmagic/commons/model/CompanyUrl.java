package webmagic.commons.model;

import lombok.Data;

import java.util.Date;

/**
 * Created by dengshougang on 16/8/4.
 */
@Data
public final class CompanyUrl{
    private String name;
    private String url;
    private Date date = new Date();
    private String keyword;
    private int pageNumber;
    private String source;
    private String infoUrl = null;
    
    public CompanyUrl(String name, String url, String keyword, int pageNumber, String infoUrl, String source){
        this.name = name;
        this.url = url;
        this.keyword = keyword;
        this.pageNumber = pageNumber;
        this.infoUrl = infoUrl;
        this.source = source;
    }
    
    public CompanyUrl(String name, String url){
        this.name = name;
        this.url = url;
    }
    
    public CompanyUrl(){
        
    }
}
    
