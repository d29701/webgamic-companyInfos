package webmagic.commons.model;

import lombok.Data;

import java.util.Date;

/**
 * Created by dengsg on 2016/8/8 0008.
 */
@Data
public final class CompanyInfo{
    private String companyName;
    private String infoUrl;
    private String establishedTime;
    private String registeredCapital;
    private String businessScope;
    private String address;
    private String mainProducts;
    private String industry;
    private String businessModel;
    private String mainCustomers;
    private String monthlyOutput;
    private String annualExports;
    private String brand;
    private String plantArea;
    private String employees;
    private String annualSales;
    private String market;
    private String source=null;
    private String legalPerson=null;
    private String companyType=null;
    private String researchPersonNumbers=null;
    private Date date=new Date();
    private String companyMainPage=null;
    private int _1688Bit=0;
    private int hc360Bit=0;
    public CompanyInfo(){
        companyName=null;
        infoUrl=null;
        establishedTime=null;
        registeredCapital=null;
        businessModel=null;
        businessScope=null;
        address=null;
        mainProducts=null;
        industry=null;
        mainCustomers=null;
        monthlyOutput=null;
        annualExports=null;
        brand=null;
        plantArea=null;
        employees=null;
    }
}
