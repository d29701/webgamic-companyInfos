package webmagic.hc360.processor;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.Jedis;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;
import webmagic.commons.model.CompanyUrl;
import webmagic.commons.util.BaiduRedrict;
import webmagic.commons.util.JedisUtil;

import java.net.URLDecoder;

import static webmagic.hc360.crawler.Hc360InfoCrawler.toInfoUrl;

/**
 * Created by dengshougang on 16/8/17.
 */
@Slf4j
public class Hc360BaiduUrlProcessor implements PageProcessor{
    private Site site = Site.me().setRetryTimes(3).setSleepTime(2000).setTimeOut(5000)
        .setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36");
    
    @Getter @Setter
    private String keyword;
    @Setter
    private String addDBName;
    @Setter
    private String remDBName;
    
    @Override
    public void process(Page page){
        try{
            int m=(int)Math.random()*2000+1000;
            Thread.currentThread().sleep(m);
        } catch (InterruptedException e){
            
        }
        
        String urlCompanyName;
        urlCompanyName=page.getUrl().get();
        urlCompanyName= URLDecoder.decode(urlCompanyName);
        urlCompanyName=urlCompanyName.substring(urlCompanyName.indexOf("hc360.com")+10,urlCompanyName.length());
        log.debug("url company name:{}",urlCompanyName);
        
        Jedis jedis;
        jedis= JedisUtil.getJedis();
        
//        jedis.srem(JedisUtil.hc360ActiveCompanyName,urlCompanyName);
//        jedis.srem(JedisUtil.hc360CalculateCompanyName,urlCompanyName);
        jedis.srem(remDBName,urlCompanyName);
        
        JedisUtil.returnJedis(jedis);
    
        
        Selectable s = page.getHtml().xpath("//*[@class=\"result c-container \"]");
        Selectable s0;
        try{
            s0 = s.nodes().get(0);
        } catch (IndexOutOfBoundsException e){
            return;
        }
    
        String baiduUrl = s0.regex("href=.*target=\"_blank\">").get();
        baiduUrl = StringUtils.remove(baiduUrl, ' ');
        baiduUrl = StringUtils.remove(baiduUrl, '"');
        baiduUrl = StringUtils.remove(baiduUrl, "href=");
        try{
            baiduUrl = baiduUrl.substring(0, baiduUrl.indexOf("target"));
        } catch (Exception e){
    
        }
        CompanyUrl companyUrl = new CompanyUrl();
        String companyName = s0.xpath("//*/em/text()").get();
//        for (String temp:s0.xpath("//*/em/text()").all()){
//            companyName+=temp;
//        }
    
//        if (!StringUtils.isBlank(baiduUrl) && baiduUrl.length()>20){
//            JedisUtil.getJedis().sadd("set_1688_company_url", baiduUrl);
//            JedisUtil.returnJedis(JedisUtil.getJedis());
//        }
        if (companyName.length()<=8){
            return;
        }
        
        companyName=StringUtils.remove(companyName,' ');
        
        companyUrl.setName(companyName);
        if (StringUtils.isBlank(this.keyword)) {
            companyUrl.setKeyword("huicong_calculate");
        } else {
            companyUrl.setKeyword(this.keyword);
        }
    
    
        jedis= JedisUtil.getJedis();
        String url=(BaiduRedrict.redrict(baiduUrl));
        companyUrl.setUrl(url);
        
        String infoUrl=toInfoUrl(url);
        jedis.sadd(addDBName,infoUrl);
        jedis.sadd(addDBName+"_copy",infoUrl);
        JedisUtil.returnJedis(jedis);
    
        companyUrl.setUrl(url);
        companyUrl.setInfoUrl(infoUrl);
    
        companyUrl.setSource("baidu.com");
        companyUrl.setPageNumber(-10);
        
        log.info("company url info:{}",companyUrl.toString());
        
        page.putField("companyUrl",companyUrl);

    }
    
    @Override
    public Site getSite(){
        return site;
    }
}
