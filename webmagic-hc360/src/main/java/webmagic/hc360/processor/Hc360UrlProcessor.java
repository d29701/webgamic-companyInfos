package webmagic.hc360.processor;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;
import webmagic.commons.model.CompanyUrl;
import webmagic.commons.util.ProxyUtil;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by dengshougang on 16/8/4.
 */
@Slf4j
public class Hc360UrlProcessor implements PageProcessor{

    private volatile boolean hasChangedProxy=false;
    
    private Site site = Site.me().setRetryTimes(3).setSleepTime(5000).setTimeOut(20000)
        .setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36")
        .addHeader("referer","http://s.webmagic.hc360.com/?w=%BB%FA%D0%B5&mc=seller&ee=4&z=%D6%D0%B9%FA%3A%B9%E3%B6%AB%CA%A1%3A%C9%EE%DB%DA%CA%D0");
    {
        ProxyUtil.changeProxy(site);
    }
    private final static String SavePath="C:\\Users\\dengsg\\Desktop\\company_info_pages\\huicong";

    private void savePage(Page page,String filename){
        File f=new File(SavePath+"//"+filename+".html");
        try{
            FileUtils.writeStringToFile(f,page.getHtml().get());
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    @Override
    public void process(Page page){
        Set set=new HashSet<>(32);
//        if (page.getHtml().get().contains("登录即表示您已同意遵守阿里巴巴服务条款")){
//            log.error("is banned!");
//            ProxyUtil.changeProxy(site);
//            return;
//        }
        Selectable h=page.getHtml();
        List<Selectable> s;
        String source="huicong";
        s=h.xpath("//*[@class=\"newCname\"]").nodes();
        
        for (int i=0;i<s.size();i++){
            String companyName=null;
            String companyUrl=null;
            String keyword=null;
            int pageNumber=-1;
            try{
                companyName = s.get(i).regex(" title=\"(.*)\">").get();
                companyUrl = s.get(i).regex("href=\"(.*)\" target=").get();
                companyName=companyName.substring(0,companyName.indexOf(">"));
                try{
                    String tempKey=page.getUrl().regex("w=(.*)&").get();
                    keyword=tempKey.substring(0,tempKey.indexOf("&"));
                    keyword=URLDecoder.decode(keyword,"utf-8");
                } catch (UnsupportedEncodingException e){
                    e.printStackTrace();
                }
                String tempNum=page.getUrl().regex("ee=\\d*").get();
                tempNum=tempNum.substring(tempNum.indexOf("=")+1);
                pageNumber=Integer.valueOf(tempNum);
            } catch (NullPointerException e){
                log.warn("#{} company info is null! {} {} {} {}",i,companyName,companyUrl,keyword,pageNumber);
                continue;
            }
            companyName=StringUtils.remove(companyName,"\"");
            int index=companyUrl.indexOf(".com");
            String infoUrl=companyUrl.substring(0,index+4)+"/shop/show.html";
            CompanyUrl company=new CompanyUrl(companyName,companyUrl,keyword ,pageNumber,infoUrl,source);
            set.add(company);
            savePage(page,companyName);
            log.info("#{} {} ",i,company.toString());
        }
        log.info("company set size: "+set.size());
        page.putField("company",set);
        if (set.size()<10){
            if (!hasChangedProxy){
                ProxyUtil.changeProxy(site);
                hasChangedProxy=true;
            }
        }
        
    }
    @Override
    public Site getSite(){
        return site;
    }
}
