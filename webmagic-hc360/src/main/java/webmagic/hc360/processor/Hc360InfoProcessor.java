package webmagic.hc360.processor;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.Jedis;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;
import webmagic.commons.model.CompanyInfo;
import webmagic.commons.model.WebPage;
import webmagic.commons.util.JedisUtil;
import webmagic.commons.util.ProxyUtil;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by dengsg on 2016/8/8 0008.
 */
@Slf4j
public class Hc360InfoProcessor implements PageProcessor{
    
    @Setter
    private String remDBName;
    
    private Site site = Site.me().setRetryTimes(3).setSleepTime(5000).setTimeOut(20000)
        .setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36")
        .addHeader("referer","http://s.webmagic.hc360.com/?w=%BB%FA%D0%B5&mc=seller&ee=4&z=%D6%D0%B9%FA%3A%B9%E3%B6%AB%CA%A1%3A%C9%EE%DB%DA%CA%D0");
    {
        ProxyUtil.changeProxy(site);
    }
    private static final String SavaPath="c:\\infopages\\huicong";

    private void savePage(Page page,String filename){
        File f=new File(SavaPath+"//"+filename+".html");
        try{
            FileUtils.writeStringToFile(f,page.getHtml().get());
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private CompanyInfo resolvePageToInfo(Page page){
        Selectable html=page.getHtml();
        CompanyInfo companyInfo=new CompanyInfo();
        String companyName=(html.xpath("//*[@id=\"companyName\"]/text()").get());
        if (StringUtils.isBlank(companyName)){
//            companyName=html.xpath("//*[@id=\"site_content\"]/div/div/div/div[2]/div/div[2]/div/div[1]/div/div[1]/h1/span/text()").get();
        }
        companyName=StringUtils.remove(companyName," ");

        companyInfo.setCompanyName(companyName);
        
        String infoUrl=page.getUrl().get();
        companyInfo.setInfoUrl(infoUrl);

        Selectable s= page.getHtml().xpath("//*[@class=\"detailsinfo\"]/table");
        
        log.debug(s.toString());
//
//        String establishedTime;
//        String registeredCapital;
//        String businessScope;
//        String address;
//        String businessModel;
//
        String mainProducts;
        StringBuilder sb=new StringBuilder();
        try{
            for (String s1:s.xpath("//td[@class=\"cLink mLink\"]/a/text()").all()){
                sb.append(StringUtils.remove(s1," "));
                sb.append(',');
            }
        }
        catch (Exception e){

        }
        try{
            mainProducts=sb.deleteCharAt(sb.length()-1).toString();
            mainProducts=StringUtils.remove(mainProducts," ");
            companyInfo.setMainProducts(mainProducts);
        } catch (Exception e){
            
        }
        
        Iterator<String> it=s.xpath("//tr/td/text()").all().iterator();
        companyInfo.setSource("huicong");
        while (it.hasNext()){
            String key=it.next();
            key=StringUtils.remove(key," ");
            key=StringUtils.remove(key,":");
            key=StringUtils.remove(key,"：");

            if (StringUtils.isBlank(key)||key.charAt(0)=='：'||key.charAt(0)==':'){
                continue;
            }

            String value=StringUtils.remove(it.next()," ");
            
            value=StringUtils.remove(value,":");
            value=StringUtils.remove(value,"：");
            
            
            if (value.equals("-")||value.equals("—")){
                value=null;
            }

            if (StringUtils.isBlank(value)){
                value=null;
            }

            if (key.contains("企业类型")){
                companyInfo.setCompanyType(value);
            } else if (key.contains("注册资本")){
                companyInfo.setRegisteredCapital(value);
            } else if (key.contains("行业")){
                companyInfo.setIndustry(value);
            } else if (key.contains("经营模式")){
                companyInfo.setBusinessModel(value);
            } else if (key.contains("经营地址")){
                companyInfo.setAddress(value);
            } else if (key.contains("法定代表人")||key.contains("法人")){
                companyInfo.setLegalPerson(value);
            } else if (key.contains("成立时间")){
                companyInfo.setEstablishedTime(value);
            } else if (key.contains("月产量")){
                companyInfo.setMonthlyOutput(value);
            } else if (key.contains("主要客户")){
                companyInfo.setMainCustomers(value);
            } else if (key.contains("年营业")){
                companyInfo.setAnnualSales(value);
            } else if (key.contains("年出口")){
                companyInfo.setAnnualExports(value);
            } else if (key.contains("品牌")){
                companyInfo.setBrand(value);
            } else if (key.contains("市场")){
                companyInfo.setMarket(value);
            } else if (key.contains("面积")){
                companyInfo.setPlantArea(value);
            } else if (key.contains("员工人数")){
                companyInfo.setEmployees(value);
            } else if (key.contains("研发部门人数")){
                companyInfo.setResearchPersonNumbers(value);
            } else if (key.contains("公司主页")){
                companyInfo.setEmployees(value);
            } else if (key.contains("加工方式")||key.contains("工艺")||key.contains("管理体系")||key.contains("是否提供加工定制")||key.contains("产品质量认证")||key.contains("代理级别")||key.contains("年进口额")||key.contains("开户银行")||key.contains("银行")||key.contains("是否提供")||key.contains("主营产品")||key.contains("质量控制")||key.contains("资信参考人")||key.contains("会员评价")||key.contains("注册地址")||key.contains("认证信息")||key.contains("证书及荣誉")){

            } else {
                log.warn("A new attribute that not contains: {} {}",key,value);
            }
        }

        return companyInfo;
    }
    private WebPage resolveWebPage(Page page){
        WebPage webPage=new WebPage();
        webPage.setStatus(page.getStatusCode());
        webPage.setSourceUrl(page.getUrl().get());
        webPage.setHtml(page.getHtml().get());
        return webPage;
    }
    
    @Override
    public void process(Page page){
    
        Jedis jedis = JedisUtil.getJedis();
        jedis.srem(remDBName,page.getUrl().get());
        JedisUtil.returnJedis(jedis);
        
        
        CompanyInfo companyInfo=resolvePageToInfo(page);
        if (companyInfo.getCompanyName()==null){
            log.warn("company name is null! ");
            ProxyUtil.changeProxy(site);
            return;
        }
        if (StringUtils.isBlank(companyInfo.getCompanyName())){
            log.warn("null pointer exception:{}",companyInfo.toString());
            return;
        }
        WebPage webPage=resolveWebPage(page);
        webPage.setName(companyInfo.getCompanyName());
        
        companyInfo.setSource("huicong");
        companyInfo.setHc360Bit((byte)1);
        
        webPage.setSource("huicong");
        
        
        
        page.putField("webPage",webPage);
        
        page.putField("companyInfo",companyInfo);
        
//        Jedis jedis = JedisUtil.getJedis();
//        jedis.srem(JedisUtil.hc360CalculateCompanyUrl,(companyInfo.getInfoUrl()));
//        JedisUtil.returnJedis(jedis);
    }
    @Override
    public Site getSite(){
        return site;
    }
}
