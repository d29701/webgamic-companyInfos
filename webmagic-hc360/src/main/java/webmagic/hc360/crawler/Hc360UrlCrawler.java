package webmagic.hc360.crawler;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import us.codecraft.webmagic.Spider;
import webmagic.commons.UrlCrawler;
import webmagic.commons.dao.CompanyUrlDao;
import webmagic.commons.downloader.SeleniumDownloader;
import webmagic.commons.model.CompanyUrl;
import webmagic.commons.pipeline.BaiduUrlPipeline;
import webmagic.commons.pipeline.CompanyUrlPipeline;
import webmagic.commons.util.JedisUtil;
import webmagic.hc360.processor.Hc360BaiduUrlProcessor;
import webmagic.hc360.processor.Hc360UrlProcessor;

import javax.annotation.Resource;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dengshougang on 16/8/4.
 */
@Slf4j
@Component
public class Hc360UrlCrawler implements UrlCrawler{
    @Resource
    private CompanyUrlPipeline companyUrlPipeline ;
    @Resource
    private BaiduUrlPipeline baiduUrlPipeline;
    @Resource
    private CompanyUrlDao companyUrlDao;
    
    
    public static final String SeleniumPath="C:\\Users\\dengsg\\Desktop\\Workspaces\\Tools\\driver";

//    public static final String SeleniumPath="/Users/dengshougang/Dropbox/Workspaces/Tools/driver/chromedriver";
    
    Spider addUrls(Spider spider,String keyword){
        return addUrls(spider,keyword,1,100);
    }
    Spider addUrls(Spider spider,String keyword,int beginpage,int endpage){
        for (int i=beginpage;i<=endpage;i++){
            spider.addUrl("http://s.webmagic.hc360.com/?w="+URLEncoder.encode(keyword)+"&mc=seller&ee="+i+"&z=%D6%D0%B9%FA%3A%B9%E3%B6%AB%CA%A1%3A%C9%EE%DB%DA%CA%D0&af=1&afadprenum=0&afadbeg=1");
            spider.addUrl("http://s.webmagic.hc360.com/?w="+URLEncoder.encode(keyword)+"&mc=seller&ee="+i+"&z=%D6%D0%B9%FA%3A%B9%E3%B6%AB%CA%A1%3A%C9%EE%DB%DA%CA%D0&af=1&afadprenum=0&afadbeg=21");
            spider.addUrl("http://s.webmagic.hc360.com/?w="+URLEncoder.encode(keyword)+"&mc=seller&ee="+i+"&z=%D6%D0%B9%FA%3A%B9%E3%B6%AB%CA%A1%3A%C9%EE%DB%DA%CA%D0&af=1&afadprenum=0&afadbeg=41");
        }
        return spider;
    }
    
    public void crawlKeyword(String keyword){
        addUrls(Spider.create(new Hc360UrlProcessor()),(keyword))
            .addPipeline(companyUrlPipeline)
            .setDownloader(new SeleniumDownloader(SeleniumPath))
            .setSpawnUrl(false)
            .thread(1).run();
    }
    
    
    @Override
    public void addCompanyNameToRedis(String redisDBName,List<CompanyUrl> ls){
        Jedis jedis= JedisUtil.getJedis();
        int i=0;
        for (CompanyUrl l : ls){
            if (l!=null&&l.getName()!=null) {
                jedis.sadd(redisDBName, l.getName());
                jedis.sadd(redisDBName+"_copy", l.getName());
//            JedisUtil.returnJedis(jedis);
            }
            i++;
            if (i % 1000==0)
                log.info("finish thousand:{}",i);
        }
    }
    
    @Override
    public void crawlRedisCompanyUrl(String redisDBName, int number, int range, boolean ifPop){
        for (int i = 0; i<number; i += range){
            Hc360BaiduUrlProcessor processor = new Hc360BaiduUrlProcessor();
            processor.setAddDBName(redisDBName+"_url");
            processor.setRemDBName(redisDBName);
            
            processor.setKeyword(StringUtils.remove(redisDBName,"set_"));
            
            Spider spider = Spider.create(processor)
                .addPipeline(baiduUrlPipeline)
                .setSpawnUrl(false)
                .thread(4);
            Jedis jedis;
            
            for (int j = 0; j<range; j++){
                jedis = JedisUtil.getJedisPool().getResource();
                String s;
                if (ifPop){
                    s=jedis.spop(redisDBName);
                }else {
                    s=(jedis.srandmember(redisDBName));
                }
                if (s==null){
                    return;
                }
                spider.addUrl("https://www.baidu.com/s?wd=site%3Ahc360.com%20"+s);
                JedisUtil.returnJedis(jedis);
            }
            
            spider.run();
        }
    }
    
    
    private static List<String> keywordSet(){
        List list=new ArrayList();
        /*
        list.add("金属加工");list.add("玩偶");list.add("文具");list.add("五金");list.add("usb线");
        list.add("移动电源");list.add("移民");list.add("机械设备");list.add("激光");
        list.add("家电");list.add("广告");list.add("医疗");list.add("药品");list.add("保健品");
        list.add("快销");list.add("家装");list.add("家居");list.add("家具");list.add("家具建材");
        list.add("工程");list.add("餐饮");list.add("文具袋");list.add("男 衬衫");
        list.add("女 衬衫");list.add("女 T恤");list.add("苹果 数据线");list.add("切削刀具");
        list.add("医疗产品");list.add("注射器");list.add("床 木");list.add("钟表");
        list.add("手机配件");list.add("洗衣机");list.add("烟花");list.add("冰箱");
        list.add("中性笔");list.add("女 安全裤");list.add("保护膜");list.add("本子");
        list.add("保健 液体");list.add("电视");list.add("女 丝袜");list.add("大码");list.add("中老年");
        list.add("连衣裙");list.add("泳装");list.add("打底裤");list.add("夹克");list.add("牛仔裤");
        list.add("文胸");list.add("内衣");list.add("女 内裤");list.add("男 内裤");list.add("睡衣");
        list.add("睡裙");list.add("女鞋");list.add("女鞋");list.add("男鞋");list.add("高跟鞋");
        list.add("童鞋");list.add("运动鞋");list.add("书包");list.add("拉杆箱");list.add("钱包");
        list.add("童装");list.add("孕妇");list.add("婴儿");list.add("玩具");list.add("保温杯");list.add("锅");list.add("碗");list.add("手办");list.add("娃娃");list.add("餐具");list.add("垃圾桶");list.add("日用品");list.add("工艺品");list.add("百货");list.add("宠物");list.add("汽车用品");list.add("食品");list.add("生鲜");list.add("家纺");list.add("家饰");list.add("家装");list.add("建材");list.add("美妆");list.add("日化");list.add("3C");list.add("手机");list.add("家电");list.add("电工");list.add("电气");list.add("安全");list.add("包装");list.add("办公");list.add("文教");list.add("照明");list.add("电子");
        list.add("机械");list.add("仪表");list.add("橡塑");list.add("化工");list.add("钢材");list.add("仿制");
        list.add("皮革");*/list.add("零食");list.add("酒水");list.add("茶叶");list.add("卫浴");list.add("毛巾");list.add("灯具");list.add("套件");list.add("五金主材");list.add("美容");list.add("护肤");list.add("化妆");list.add("护理");list.add("清洁");list.add("美发");list.add("成人");list.add("手机配件");list.add("电脑配件");list.add("数码配件");list.add("厨房电器");list.add("家庭电器");list.add("影音");list.add("监控");list.add("防盗");list.add("办公用品");list.add("礼品");list.add("包装");list.add("照明");list.add("电子市场");list.add("机械主机");list.add("机械配件");list.add("仪器");list.add("面料");list.add("辅料");list.add("皮革");
        return list;
    }
    
    /*
    public void baiduUrlToUrl_Redis(){
        for (int i = 0; i<1000; i++){
            Jedis jedis=JedisUtil.getJedis();
            String s=jedis.spop(JedisUtil.hc360ActiveCompanyBaiduUrl);
            s=toInfoUrl(BaiduRedrict.redrict(s));
            jedis.sadd(JedisUtil.hc360CalculateCompanyUrl,s);
            jedis.sadd(JedisUtil.hc360CalculateCompanyUrlCopy,s);
            JedisUtil.returnJedis(jedis);
        }
    }
    */
    
    public static void main(String[] args){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/spring/applicationContext*.xml");
        final Hc360UrlCrawler crawler = applicationContext.getBean(Hc360UrlCrawler.class);
    
    
        int index=1000000;
        int range=100000;
        log.info("add name to redis:{}-{}",index,index+range);
//        List<CompanyUrl> ls=crawler.companyUrlDao.selectAllCompanyName(index,range);
//        crawler.addCompanyNameToRedis(JedisUtil.hc360_3rdCalculateCompanyName,ls);
        
        crawler.crawlRedisCompanyUrl(JedisUtil.hc360_3rdCalculateCompanyName,90000,100,false);
    }
    
}
