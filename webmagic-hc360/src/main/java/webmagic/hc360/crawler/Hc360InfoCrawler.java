package webmagic.hc360.crawler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import us.codecraft.webmagic.Spider;
import webmagic.commons.InfoCrawler;
import webmagic.commons.dao.CompanyInfoDao;
import webmagic.commons.dao.CompanyUrlDao;
import webmagic.commons.pipeline.CompanyInfoPipeline;
import webmagic.commons.util.JedisUtil;
import webmagic.hc360.processor.Hc360InfoProcessor;

import javax.annotation.Resource;

/**
 * Created by dengsg on 2016/8/8 0008.
 */
@Slf4j
@Component
public class Hc360InfoCrawler implements InfoCrawler{
    @Resource
    private CompanyUrlDao companyUrlDao;
    @Resource
    private CompanyInfoDao companyInfoDao;
    @Resource
    private CompanyInfoPipeline companyInfoPipeline;

    public  static String toInfoUrl(String url){
        int index=url.indexOf(".com");
        String infoUrl=url.substring(0,index+4)+"/shop/show.html";
//        companyUrlDao.updateInfoUrl(url,infoUrl);
        return infoUrl;
    }
    
    
    @Override
    public void crawlRedisCompanyInfos(String redisDBName,int number,int range,boolean ifPop){
        if (redisDBName.endsWith("_url")){
            
        }else {
            redisDBName+="_url";
        }
        for (int i = 0; i<number; i+=range){
            Hc360InfoProcessor processor=new Hc360InfoProcessor();
            processor.setRemDBName(redisDBName);
            
            Spider spider=Spider.create(processor);
            Jedis jedis;
            for (int j = 0; j<range; j++){
                jedis = JedisUtil.getJedis();
                String url;
                if (ifPop) {
                    url = jedis.spop(redisDBName);
                } else {
                    url = jedis.srandmember(redisDBName);
                }
                
                
                JedisUtil.returnJedis(jedis);
                
                if ((url)==null){
                    return;
                }
                spider.addUrl(url);
            }
            try{
                spider.addPipeline(companyInfoPipeline)
//                .setDownloader(new HtmlUnitDownloader())
//                .setDownloader(new SeleniumDownloader(CompanyUrlCrawler.SeleniumPath))
                    .setSpawnUrl(false)
                    .thread(3).run();
            } catch (Exception e){
                e.printStackTrace();
                return;
            }
            
            companyInfoDao.updateAllHuicongCompanyBit();
            
            log.info("webmagic.hc360 crawl info finish:{}-{}",i,i+range);
        }
    }
    
   
    
    public static void main(String[] args){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/spring/applicationContext*.xml");
        Hc360InfoCrawler crawler=applicationContext.getBean(Hc360InfoCrawler.class);
        
        
        crawler.crawlRedisCompanyInfos(JedisUtil.hc360_3rdCalculateCompanyName,8000,100,false);
    }
}
