package webmagic._1688.crawler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import us.codecraft.webmagic.Spider;
import webmagic._1688.processor._1688InfoProcessor;
import webmagic.commons.InfoCrawler;
import webmagic.commons.dao.CompanyInfoDao;
import webmagic.commons.pipeline.CompanyInfoPipeline;
import webmagic.commons.util.JedisUtil;
import javax.annotation.Resource;

/**
 * Created by dengsg on 2016/8/8 0008.
 */
@Component
@Slf4j
public class _1688InfoCrawler implements InfoCrawler{

    @Resource
    private CompanyInfoPipeline companyInfoPipeline;
    @Resource
    private CompanyInfoDao companyInfoDao;
    
    
    public static synchronized String toInfoUrl(String url){
        int index=url.indexOf(".com");
        if (index<=0){
            return url;
        }
        String infoUrl=url.substring(0,index+4)+"/page/creditdetail.html";
//        companyUrlDao.updateInfoUrl(url,infoUrl);
        return infoUrl;
    }
    
    public void crawlRedisCompanyInfos(String redisDBName,int number, int range,boolean ifPop){
        
        redisDBName+="_url";
        
        for (int i = 0; i<number; i+=range){
            _1688InfoProcessor processor=new _1688InfoProcessor();
//            processor.setRemDBName(redisDBName);
            
            Spider spider=Spider.create(processor)
                .setSpawnUrl(false)
                .addPipeline(companyInfoPipeline)
                .thread(5);
            Jedis jedis = JedisUtil.getJedisPool().getResource();
    
            int m= Math.toIntExact(jedis.scard(redisDBName));
            if (m<=range){
                ifPop=true;
            }
            for (int j = 0; j<range; j++){
                jedis=JedisUtil.getJedis();
                String url;
                if (ifPop){
                    url = jedis.spop(redisDBName);
                }else {
                    url=jedis.srandmember(redisDBName);
                }
                JedisUtil.returnJedis(jedis);
                if ((url)==null){
                    return;
                }
                spider.addUrl(url);
            }
            try{
                spider.run();
            }
            catch (Exception e){
                e.printStackTrace();
                continue;
            }
            companyInfoDao.updateAll1688CompanyBit1();
            log.info("crawl finish: {}-{}",i,i+range);
        }
    }
    
    //先将公司名加入到jedis中,然后爬取url存入jedis,然后爬取info信息存入mysql中的company_info表

    public static void main(String[] args){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/spring/applicationContext*.xml");
        _1688InfoCrawler crawler = applicationContext.getBean(_1688InfoCrawler.class);
        
//        crawler.crawlRedisCompanyInfos(JedisUtil._1688_3rdCalculateCompanyName,100000,100,false);
        crawler.crawlRedisCompanyInfos(JedisUtil._1688_3rdCalculateCompanyName,100000,400,false);
        
    }
}
