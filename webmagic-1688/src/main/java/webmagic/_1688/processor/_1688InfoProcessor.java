package webmagic._1688.processor;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.Jedis;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;
import webmagic.commons.model.CompanyInfo;
import webmagic.commons.model.WebPage;
import webmagic.commons.util.JedisUtil;
import webmagic.commons.util.ProxyUtil;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by dengsg on 2016/8/8 0008.
 */
@Slf4j
public class _1688InfoProcessor implements PageProcessor{
    @Setter
    private String remDBName;
    
    private Site site=Site.me().setRetryTimes(3).setSleepTime(5000).setTimeOut(5000).setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.1650.57 Safari/537.36");
    {
        ProxyUtil.changeProxy(site);
    }
    private static final String SavaPath="c:\\infopages";

    private void savePage(Page page,String filename){
        File f=new File(SavaPath+"//"+filename+".html");
        try{
            FileUtils.writeStringToFile(f,page.getHtml().get());
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private CompanyInfo resolvePageToInfo(Page page){
        Selectable html=page.getHtml();
        CompanyInfo companyInfo=new CompanyInfo();
        String companyName=(html.xpath("//*[@id=\"site_content\"]/div/div/div/div[2]/div/div[2]/div/div[1]/div/div/h1/span/text()").get());
        if (StringUtils.isBlank(companyName)){
            companyName=html.xpath("//*[@id=\"site_content\"]/div/div/div/div[2]/div/div[2]/div/div[1]/div/div[1]/h1/span/text()").get();
        }
    
        companyName=webmagic.commons.util.StringUtils.replaceBlank(companyName);
    
        String infoUrl=page.getUrl().get();
        String establishedTime=html.xpath("//*[@id=\"site_content\"]/div/div/div/div[2]/div/div[2]/div/div[2]/div[2]/table/tbody/tr[1]/td[2]/p/span[1]/text()").get();
        String registeredCapital=html.xpath("//*[@id=\"site_content\"]/div/div/div/div[2]/div/div[2]/div/div[2]/div[2]/table/tbody/tr[2]/td[2]/p/span[1]/text()").get();
        String businessScope=html.xpath("//*[@id=\"site_content\"]/div/div/div/div[2]/div/div[2]/div/div[2]/div[2]/table/tbody/tr[3]/td[2]/p/span[1]/text()").get();
        String address=html.xpath("//*[@id=\"site_content\"]/div/div/div/div[2]/div/div[2]/div/div[2]/div[2]/table/tbody/tr[4]/td[2]/p/span/text()/text()").get();
        String businessModel=html.xpath("//*[@id=\"J_CompanyDetailInfoList\"]/div[1]/table/tbody/tr[2]/td[4]/text()").get();
        companyInfo.setCompanyName(companyName);
        companyInfo.setInfoUrl(infoUrl);
        companyInfo.setEstablishedTime(establishedTime);
        companyInfo.setRegisteredCapital(registeredCapital);
        companyInfo.setBusinessScope(StringUtils.remove(businessScope,"^"));
        companyInfo.setAddress(address);
        companyInfo.setBusinessModel(businessModel);
        Selectable infoBody=page.getHtml().xpath("//*[@id=\"J_CompanyDetailInfoList\"]");
        List<String> keys=infoBody.xpath("//*[@class=\'data-key\']/text()").all();
        List<String> values=infoBody.xpath("//*[@class=\'data-value\']/text()").all();
        Iterator<String> it=values.iterator();
        for (String key:keys){
            String value=it.next();
            value=StringUtils.remove(value,' ');
            value=StringUtils.remove(value," ");
            if (key.contains("资金")){
                companyInfo.setRegisteredCapital(value);
            } else if (key.contains("主")&&key.contains("产品")){
                companyInfo.setMainProducts(value);
            } else if (key.contains("行业")){
                companyInfo.setIndustry(value);
            } else if (key.contains("经营模式")){
                companyInfo.setBusinessModel(value);
            }  else if (key.contains("客户")){
                companyInfo.setMainCustomers(value);
            } else if (key.contains("月产量")){
                companyInfo.setMonthlyOutput(value);
            } else if (key.contains("年营业")){
                companyInfo.setAnnualSales(value);
            } else if (key.contains("年出口")){
                companyInfo.setAnnualExports(value);
            } else if (key.contains("品牌")){
                companyInfo.setBrand(value);
            } else if (key.contains("市场")){
                companyInfo.setMarket(value);
            } else if (key.contains("面积")){
                companyInfo.setPlantArea(value);
            } else if (key.contains("人数")){
                companyInfo.setEmployees(value);
            } else if (key.contains("加工方式")||key.contains("工艺")||key.contains("管理体系")||key.contains("是否提供加工定制")||key.contains("产品质量认证")||key.contains("代理级别")){
//                companyInfo.set;
                
//                continue;
            } else {
                log.warn("A new attribute that not contains: {} {}",key,value);
            }
        }
        return companyInfo;
    }
    
    
    private WebPage resolveWebPage(Page page){
        WebPage webPage=new WebPage();
        webPage.setStatus(page.getStatusCode());
        webPage.setSourceUrl(page.getUrl().get());
        webPage.setHtml(page.getHtml().get());
        return webPage;
    }
    
    private CompanyInfo resolveType2Info(Page page){
        Selectable html=page.getHtml();
        CompanyInfo companyInfo=new CompanyInfo();
        String companyName=(html.xpath("//*[@class=\"company-name\"]/text()").get());
        if (StringUtils.isBlank(companyName)){
            return null;
        }
        String infoUrl=page.getUrl().get();
        
        List<Selectable> ls=page.getHtml().xpath("//*[@class=\"content-info\"]").nodes();
        
        companyName=webmagic.commons.util.StringUtils.replaceBlank(companyName);
        
        companyInfo.setCompanyName(companyName);
        companyInfo.setInfoUrl(infoUrl);
        
        for (Selectable info:ls){
            String key=info.xpath("//*[@class=\"title\"]/p/text()").get();
            key=StringUtils.remove(key,' ');
            key=StringUtils.remove(key," ");
            key=StringUtils.remove(key,':');
            key=StringUtils.remove(key,":");
            key=webmagic.commons.util.StringUtils.replaceBlank(key);
    
            String value=info.xpath("//*[@class=\"info\"]/p/text()").get();
            value=StringUtils.remove(value,' ');
            value=StringUtils.remove(value," ");
            value=webmagic.commons.util.StringUtils.replaceBlank(value);
    
            if (key.contains("注册资")){
                companyInfo.setRegisteredCapital(value);
            } else if (key.contains("主")&&key.contains("产品")){
                companyInfo.setMainProducts(value);
            } else if (key.contains("行业")){
                companyInfo.setIndustry(value);
            } else if (key.contains("经营模式")){
                companyInfo.setBusinessModel(value);
            }  else if (key.contains("客户")){
                companyInfo.setMainCustomers(value);
            } else if (key.contains("月产量")){
                companyInfo.setMonthlyOutput(value);
            } else if (key.contains("年营业")){
                companyInfo.setAnnualSales(value);
            } else if (key.contains("年出口")){
                companyInfo.setAnnualExports(value);
            } else if (key.contains("品牌")){
                companyInfo.setBrand(value);
            } else if (key.contains("市场")){
                companyInfo.setMarket(value);
            } else if (key.contains("面积")){
                companyInfo.setPlantArea(value);
            } else if (key.contains("人数")){
                companyInfo.setEmployees(value);
            } else if (key.contains("加工方式")||key.contains("工艺")||key.contains("管理体系")||key.contains("是否提供加工定制")||key.contains("产品质量认证")||key.contains("代理级别")){
                
            } else {
                log.warn("A new attribute that not contains: {} {}",key,value);
            }
        }
        return companyInfo;
    }
    
    @Override
    public void process(Page page){
        Selectable html;
        html=page.getHtml();
        if (html.get().contains("登录即表示您已同意遵守阿里巴巴服务条款")){
//            ProxyUtil.changeProxy(site);
//            log.info(html.get());
            return;
        }
        CompanyInfo companyInfo;
        if (page.getUrl().get().contains("creditdetail")){
            companyInfo = resolvePageToInfo(page);
        }
        else {
            companyInfo=resolveType2Info(page);
        }
//        savePage(page,companyInfo.getCompanyName());
        WebPage webPage=resolveWebPage(page);
        webPage.setName(companyInfo.getCompanyName());
        
        companyInfo.setSource("1688");
        
        companyInfo.set_1688Bit(1);
        
        webPage.setSource("1688");
        
        page.putField("webPage",webPage);
        
//        log.info("company info: {}",companyInfo.toString());
        page.putField("companyInfo",companyInfo);
    
        
        Jedis jedis= JedisUtil.getJedis();
        jedis.srem(remDBName,page.getUrl().get());
        JedisUtil.returnJedis(jedis);
        
    }
    @Override
    public Site getSite(){
        return site;
    }
}
