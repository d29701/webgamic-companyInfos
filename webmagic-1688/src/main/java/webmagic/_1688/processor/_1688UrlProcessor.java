package webmagic._1688.processor;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;
import webmagic.commons.model.CompanyUrl;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dengshougang on 16/8/4.
 */

@Slf4j
public class _1688UrlProcessor implements PageProcessor{
    
    private Site site=Site.me().setRetryTimes(3).setSleepTime(8000).setTimeOut(5000).setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.1650.57 Safari/537.36");
    {
//        ProxyUtil.changeProxy(site);
    }
    private final static String SavePath="C:\\Users\\dengsg\\Desktop\\company_info_pages";

    private void savePage(Page page,String fileName){
        File file=new File(SavePath+"\\"+fileName);
    }
    
    
    @Override
    public void process(Page page){
        Selectable s;
        Set set=new HashSet<>(32);
        for (int i=1;i<=20;i++){
            s=page.getHtml().xpath("//*[@id=\"offer"+i+"\"]/div[2]/div[4]/a");
            String companyName=null;
            String companyUrl=null;
            String keyword=null;
            int pageNumber=-1;
            try{
                companyName = s.regex("\"> (.*) </a>").get();
                companyUrl = s.regex("href=(.*)\" offer-stat").get();
                companyUrl=companyUrl.substring(1);
                try{
                    String tempKey=page.getUrl().regex("keywords=(.*)&").get();
                    keyword=tempKey.substring(0,tempKey.indexOf("&"));
                    keyword=URLDecoder.decode(keyword,"utf-8");
                } catch (UnsupportedEncodingException e){
                    e.printStackTrace();
                }
                String tempNum=page.getUrl().regex("beginPage=\\d*").get();
                tempNum=tempNum.substring(tempNum.indexOf("=")+1);
                pageNumber=Integer.valueOf(tempNum);
            } catch (NullPointerException e){
                log.warn("#{} company info is null! {} {} {} {}",i,companyName,companyUrl,keyword,pageNumber);
                continue;
            }
            companyName= StringUtils.remove(companyName,"\"");
            CompanyUrl company=new CompanyUrl();
            company.setName(companyName);
            company.setUrl(companyUrl);
            company.setKeyword(keyword);
            company.setPageNumber(pageNumber);
            company.setSource("1688");
            set.add(company);
            log.info("#{} {} {} {} {}",i,companyName,companyUrl,keyword,pageNumber);
        }
        log.warn("company set size: "+set.size());
        page.putField("company",set);

        //
    }
    
    @Override
    public Site getSite(){
        return site;
    }
}
