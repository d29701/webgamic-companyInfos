package webmagic._1688.processor;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.Jedis;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;
import webmagic.commons.model.CompanyUrl;
import webmagic.commons.util.BaiduRedrict;
import webmagic.commons.util.JedisUtil;

import java.net.URLDecoder;

import static webmagic._1688.crawler._1688InfoCrawler.toInfoUrl;

/**
 * Created by dengshougang on 16/8/17.
 */
@Slf4j
public class _1688BaiduUrlProcessor implements PageProcessor{
    
    @Getter @Setter
    private String keyword;
    @Getter @Setter
    private String remDBName;
    @Getter @Setter
    private String addDBName;
    
    private Site site = Site.me().setRetryTimes(2).setSleepTime(2000).setTimeOut(5000)
        .setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36");
    
    @Override
    public void process(Page page){
        try{
            int m=(int)Math.random()*2000+1000;
            Thread.currentThread().sleep(m);
        } catch (InterruptedException e){

        }
        
        String urlCompanyName;
        urlCompanyName=page.getUrl().get();
        urlCompanyName= URLDecoder.decode(urlCompanyName);
        urlCompanyName=urlCompanyName.substring(urlCompanyName.indexOf("1688.com")+9,urlCompanyName.length());
        log.debug("url company name:{}",urlCompanyName);
    
        
        Jedis jedis;
        jedis= JedisUtil.getJedis();
        jedis.srem(remDBName,urlCompanyName);
        JedisUtil.returnJedis(jedis);
        
        Selectable s = page.getHtml().xpath("//*[@class=\"result c-container \"]");
        Selectable s0;
        try{
            s0 = s.nodes().get(0);
        } catch (IndexOutOfBoundsException e){
            return;
        }
    
        String baiduUrl = s0.regex("href=.*target=\"_blank\">").get();
        baiduUrl = StringUtils.remove(baiduUrl, ' ');
        baiduUrl = StringUtils.remove(baiduUrl, '"');
        baiduUrl = StringUtils.remove(baiduUrl, "href=");
        try{
            baiduUrl = baiduUrl.substring(0, baiduUrl.indexOf("target"));
        } catch (Exception e){
    
        }
        CompanyUrl companyUrl = new CompanyUrl();
        String companyName = s0.xpath("//*/em/text()").get();
        
    
        if (companyName==null||companyName.length()<=8){
            return;
        }
        
        companyName=StringUtils.remove(companyName,' ');
        
        
        String url=toInfoUrl(BaiduRedrict.redrict(baiduUrl));
        
        companyUrl.setUrl(url);
        companyUrl.setName(companyName);
    
        if (StringUtils.isBlank(this.keyword)) {
            companyUrl.setKeyword("1688");
        } else {
            companyUrl.setKeyword(this.keyword);
        }
        
        companyUrl.setSource("baidu.com");
        
        companyUrl.setPageNumber(-10);
        
        log.info("company url info:{}",companyUrl.toString());
        
        jedis=JedisUtil.getJedis();
        jedis.sadd(addDBName,companyUrl.getUrl());
        jedis.sadd(addDBName+"_copy",companyUrl.getUrl());
        JedisUtil.returnJedis(jedis);
        
        page.putField("companyUrl",companyUrl);
        
    }
    
    
    @Override
    public Site getSite(){
        return site;
    }
}
